# Amortizer

An Android 10 app to build amortization tables and graphs. Also shows the effect of paying loans off early vs investing the loan payment.
