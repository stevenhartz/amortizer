package com.hartzconsult.amortizer

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.preference.PreferenceManager

class LoanData(private val sharedPref: SharedPreferences, private val context: Context) {

    private val TAG = "LoanData"

    class LoanDataDAO(
        val principal: Double? = null,
        val interestRate: Double? = null,
        val terms: Double? = null,
        val extraPayment: Double? = null
    )

    private fun saveLoanOk(): Boolean{
        return sharedPref.getBoolean(context.resources.getString(R.string.sp_save_loan_info), false)
    }

    fun purgeLoanData(){
        with(sharedPref.edit()){
            remove(context.resources.getString(R.string.sp_auto_principal))
            remove(context.resources.getString(R.string.sp_auto_interest))
            remove(context.resources.getString(R.string.sp_auto_term))
            remove(context.resources.getString(R.string.sp_auto_extra_pmt))

            commit()
        }
    }

    fun retrieveSavedData(mode: String): LoanDataDAO {
        // If we aren't saving any data, we shouldn't retrieve anything either.
        if(saveLoanOk()){
            when(mode) {
                context.resources.getString(R.string.form_mode_auto) -> {
                    // Can't save anything as doubles, so we need to do strings and convert. :(
                    val principal = sharedPref.getString(context.resources.getString(R.string.sp_auto_principal), null)
                    val rate = sharedPref.getString(context.resources.getString(R.string.sp_auto_interest), null)
                    val term = sharedPref.getString(context.resources.getString(R.string.sp_auto_term), null)
                    val extraPmt = sharedPref.getString(context.resources.getString(R.string.sp_auto_extra_pmt), null)

                    return LoanDataDAO(
                        principal?.toDoubleOrNull(),
                        rate?.toDoubleOrNull(),
                        term?.toDoubleOrNull(),
                        extraPmt?.toDoubleOrNull()
                    )
                }

                context.resources.getString(R.string.form_mode_mortgage) -> {
                    // Can't save anything as doubles, so we need to do strings and convert. :(
                    val principal = sharedPref.getString(context.resources.getString(R.string.sp_mortgage_principal), null)
                    val rate = sharedPref.getString(context.resources.getString(R.string.sp_mortgage_interest), null)
                    val term = sharedPref.getString(context.resources.getString(R.string.sp_mortgage_term), null)
                    val extraPmt = sharedPref.getString(context.resources.getString(R.string.sp_mortgage_extra_pmt), null)

                    return LoanDataDAO(
                        principal?.toDoubleOrNull(),
                        rate?.toDoubleOrNull(),
                        term?.toDoubleOrNull(),
                        extraPmt?.toDoubleOrNull()
                    )
                }

                else -> {
                    return LoanDataDAO()
                }
            }
        }else{
            return LoanDataDAO()
        }
    }

    fun saveLoanData(mode: String, loanData: LoanDataDAO){

        Log.d(TAG, "Attempting to save data")

        // quite likely a double-check, but let's idiot-proof this:
        if(saveLoanOk()){
            Log.d(TAG, "Saving is allowed, proceeding")
            when(mode) {
                context.resources.getString(R.string.form_mode_auto) -> {

                    with(sharedPref.edit()) {
                        // Can't save anything as doubles, so we need to do strings and convert. :(
                        putString(context.resources.getString(com.hartzconsult.amortizer.R.string.sp_auto_principal), loanData.principal.toString())
                        putString(context.resources.getString(com.hartzconsult.amortizer.R.string.sp_auto_interest), loanData.interestRate.toString())
                        putString(context.resources.getString(com.hartzconsult.amortizer.R.string.sp_auto_term), loanData.terms.toString())
                        putString(context.resources.getString(com.hartzconsult.amortizer.R.string.sp_auto_extra_pmt), loanData.extraPayment.toString())

                        commit()
                    }
                }

                context.resources.getString(R.string.form_mode_mortgage) -> {
                    with(sharedPref.edit()) {
                        // Can't save anything as doubles, so we need to do strings and convert. :(
                        putString(context.resources.getString(com.hartzconsult.amortizer.R.string.sp_mortgage_principal), loanData.principal.toString())
                        putString(context.resources.getString(com.hartzconsult.amortizer.R.string.sp_mortgage_interest), loanData.interestRate.toString())
                        putString(context.resources.getString(com.hartzconsult.amortizer.R.string.sp_mortgage_term), loanData.terms.toString())
                        putString(context.resources.getString(com.hartzconsult.amortizer.R.string.sp_mortgage_extra_pmt), loanData.extraPayment.toString())

                        commit()
                    }

                }

                else -> return
            }
        }
    }
}