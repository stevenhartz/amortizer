package com.hartzconsult.amortizer

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceManager
import java.lang.ClassCastException
import java.lang.Exception
import java.lang.NullPointerException
import kotlin.math.abs
import kotlin.math.ceil
import kotlin.math.roundToInt


class LoanFormFragment : Fragment() {

    private val TAG = "LoanForm"

    // Specifically for use in XML:
    private var mode: String? = null

    // =============================
    // DO NOT use the following in XML
    private lateinit var defaultMode: String

    private lateinit var principalNumberEdit: EditText
    private lateinit var rateNumberEdit : EditText
    private lateinit var termNumberEdit : EditText
    private lateinit var extraPmtEdit : EditText
    private lateinit var showAmortizationTable: Button
    private lateinit var tempButton : Button

    // un-rounded payment (makes amortization table work out cleanly)
    private var payment = 0.0
    private var shortTermTaxRate = 0.0
    private var taxRate = 0.0

    private lateinit var newTermTextView : TextView
    private lateinit var paymentTextView : TextView
    private lateinit var youPayMoneyTextView: TextView
    private lateinit var youSaveMoneyTextView: TextView
    private lateinit var titleText: TextView
    private lateinit var termText: TextView
    private lateinit var investInstead: TextView

    private lateinit var sharedPref : SharedPreferences

    private lateinit var myView: View

    private var saveLoanInfo: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_loan_form, container, false)
        defaultMode = resources.getString(R.string.form_default_mode)

        Log.d(TAG, mode?.let{
            "[onCreateView] mode value was $it"
        } ?:
            "[onCreateView] mode value was null, assuming $defaultMode"
        )

        return myView
    }



    override fun onInflate(context: Context, attrs: AttributeSet, savedInstanceState: Bundle?) {
        super.onInflate(context, attrs, savedInstanceState)

        val xmlAttrs = context.obtainStyledAttributes(attrs, R.styleable.LoanFragment_MemberInjector)
        mode = xmlAttrs.getString(R.styleable.LoanFragment_MemberInjector_mode)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        Log.d(TAG, mode?.let{
            "[onActivityCreated] mode value was $it"
        } ?:
            "[onActivityCreated] mode value was null, assuming $defaultMode"
        )

        getViewReferences()

        setupEventHandlers()

    }

    override fun onResume() {
        super.onResume()

        // Read sharedPrefs here because going to the settings menu only triggers onPause/onResume,
        // not activity created, etc.
        sharedPref = PreferenceManager.getDefaultSharedPreferences(requireContext())

        saveLoanInfo = sharedPref.getBoolean(resources.getString(R.string.sp_save_loan_info), false)

        val defaultTaxRate = resources.getInteger(R.integer.default_tax_rate)

        taxRate = try {
            val s = sharedPref.getString(resources.getString(R.string.sp_tax_rate), null)
            (s!!.subSequence(1, s.length).toString()).toDouble() / 100.0
        }
        catch( e: Throwable){

            // Catches our "Unset" case
            when(e) {
                is NullPointerException, is ClassCastException, is NumberFormatException -> {

                    Log.d(TAG, "caught NPE or ClassCast")
                    defaultTaxRate.toDouble() / 100.0
                }

                else -> throw e
            }
        }

        if(taxRate < 0)
            taxRate = defaultTaxRate / 100.0


        setStringsByMode()
        calcPaymentFromNumbers()
    }


    private fun getViewReferences(){
        principalNumberEdit = myView.findViewById(R.id.principalNumberEdit)
        rateNumberEdit = myView.findViewById(R.id.rateEditText)
        termNumberEdit = myView.findViewById(R.id.termEditText)
        paymentTextView = myView.findViewById(R.id.paymentTextView)
        extraPmtEdit = myView.findViewById(R.id.extraPmtEditText)
        showAmortizationTable = myView.findViewById(R.id.showAmortizationTableButton)
        newTermTextView = myView.findViewById(R.id.newTermText)
        youPayMoneyTextView = myView.findViewById(R.id.youPayMoney)
        youSaveMoneyTextView = myView.findViewById(R.id.youSaveMoney)
        investInstead = myView.findViewById(R.id.investInstead)

        titleText = myView.findViewById(R.id.titleText)
        termText = myView.findViewById(R.id.termText)
    }


    private fun setupEventHandlers(){
        // Make these into values so we can reuse them
        val onFocusHandler = View.OnFocusChangeListener { _, _ ->
            saveData()
            updatePayment()
        }

        val onFocusHandlerPretty = View.OnFocusChangeListener { _, _ ->
            principalNumberEdit.setText(prettyNumber(principalNumberEdit.text.toString(), false))
            saveData()
            updatePayment()
        }

        principalNumberEdit.onFocusChangeListener = onFocusHandlerPretty
        rateNumberEdit.onFocusChangeListener = onFocusHandler
        termNumberEdit.onFocusChangeListener = onFocusHandler
        extraPmtEdit.onFocusChangeListener = onFocusHandlerPretty

        principalNumberEdit.addTextChangedListener(addSeparatorTextWatcher)
        extraPmtEdit.addTextChangedListener(addSeparatorTextWatcher)
        rateNumberEdit.addTextChangedListener(recalculateTextWatcher)
        termNumberEdit.addTextChangedListener(recalculateTextWatcher)


        showAmortizationTable.setOnClickListener {
            val data = Bundle().apply {
                val principal =
                    numberRemoveSeperators(principalNumberEdit.text.toString()).toDouble()
                this.putDouble(AmortizationTableFragment.INTENT_PRINCIPAL, principal)
                Log.d("Mort Form Nav", "Principal = $principal")
                this.putDouble(
                    AmortizationTableFragment.INTENT_RATE,
                    editTextToDouble(rateNumberEdit)
                )
                this.putDouble(
                    AmortizationTableFragment.INTENT_TERMS,
                    editTextToDouble(termNumberEdit)
                )

                // Fixes a weird bug where this.payment isn't updated when form first created.
                // Problem didn't exist when this was an activity, I don't know what's going on here...
                calcPaymentFromNumbers()

                val extraPmt = editTextToDouble(extraPmtEdit)
                this.putDouble(AmortizationTableFragment.INTENT_PAYMENT, payment + extraPmt)
            }

            val navController = this.findNavController()
            navController.navigate(R.id.amortizationTableFragment, data)
        }

    }

    private fun saveData(){

        val data = getNumbersFromForm()

        LoanData(sharedPref, requireContext()).saveLoanData(mode ?: defaultMode, data)
    }

    private fun editTextToDouble(e: EditText): Double{
        val safechars = e.text.toString().replace(Regex("[^0-9.]"), "")
        return safechars.toDouble()
    }

    @Deprecated(message = "Double think this, often leads to round-off issues after String.Format")
    private fun textViewToDouble(e: TextView): Double{
        val safechars = e.text.replace(Regex("[^0-9.]"), "")
        return safechars.toDouble()
    }

    private fun updatePayment(){
        calcPaymentFromNumbers()?.let {
            paymentTextView.text =
                getString(R.string.currency_symbol) + String.format("%.2f", it)
        }
    }

    private fun numberRemoveSeperators(num : String): String{
        return num.replace(",", "")
    }

    private fun getMonthsPerTermUnit(): Int{
        return when(mode ?: defaultMode){
            resources.getString(R.string.form_mode_auto) -> {
                resources.getInteger(R.integer.form_auto_months_per_term_unit)
            }

            resources.getString(R.string.form_mode_mortgage) -> {
                resources.getInteger(R.integer.form_mortgage_months_per_term_unit)
            }

            else -> {
                Log.w(TAG, "Unrecognized mode, making an assumption about months per term unit")
                12
            }
        }
    }

    private fun getTermUnitQuantityString(qty: Int): String{
        return when(mode ?: defaultMode){
            resources.getString(R.string.form_mode_auto) -> {
                resources.getQuantityString(R.plurals.form_auto_term_unit, qty)
            }

            resources.getString(R.string.form_mode_mortgage) -> {
                resources.getQuantityString(R.plurals.form_mortgage_term_unit, qty)
            }

            else -> {
                Log.w(TAG, "Unrecognized mode, making an assumption about 'term unit' name")
                "months"
            }
        }
    }

    private fun getNumbersFromForm(): LoanData.LoanDataDAO{

        val principal = try {
            numberRemoveSeperators(principalNumberEdit.text.toString()).toDouble()
        } catch (e: Exception) {
            Log.d("calc", "Problem getting principal value as doubles")
            null
        }

        val rate = try {
            editTextToDouble(rateNumberEdit)
        } catch (e: Exception) {
            Log.d("calc", "Problem getting rate value as doubles")
            null
        }

        val termUnits = try {
            editTextToDouble(termNumberEdit)
        } catch (e: Exception) {
            Log.d("calc", "Problem getting terms value as doubles")
            null
        }

        val extraPmt = try {
            editTextToDouble(extraPmtEdit)
        } catch (e: Exception) {
            Log.d("calc", "Problem getting extra payment value as doubles")
            null
        }


        return LoanData.LoanDataDAO(
            principal,
            rate,
            termUnits,
            extraPmt
        )
    }

    private fun calcPaymentFromNumbers() : Double?{

        Log.d("calcPaymentFromNumbers", "Running")

        val monthsPerTermUnit = getMonthsPerTermUnit()

        val data = getNumbersFromForm()
        val principal = data.principal ?: return null
        val rate = data.interestRate?.div(100.0) ?: return null
        val termUnits = data.terms ?: return null
        val extraPmt = data.extraPayment ?: return null


        Log.d("calc", "Returning calculation")
        // TODO: is it always ok to assume APR is based on Monthly interest?
        // Note: rate/12 is only correct for monthly interest!
        payment = calcPayment(principal, rate/12.0, termUnits * monthsPerTermUnit.toDouble())
        if(payment.isNaN()){
            // Divide by zero happened
            return null
        }



        val termAfterExtra = calcTerm(principal, rate/12.0, payment + extraPmt)

        if(termAfterExtra == null){
            // Divide by zero happened
            return null
        }

        // "if" fixes minor roundoff errors
        val intTermAfterExtra = if(abs(termAfterExtra - termAfterExtra.roundToInt()) < 0.01){
            termAfterExtra.roundToInt()
        }else{
            ceil(termAfterExtra).roundToInt()
        }



        // Term is always a whole number of periods (months).
        val fullTermUnits = intTermAfterExtra / monthsPerTermUnit
        val months = intTermAfterExtra % monthsPerTermUnit

        // But, that messes up "periods * payment" math and causes strange effects
        // like paying $1 more costs extra
        val maxTotalPayments = payment * termUnits * monthsPerTermUnit
        val realTotalPayments = (payment + extraPmt) * termAfterExtra


        val investReturn = calcInvestmentWithTax(extraPmt, extraPmt, 0.07, intTermAfterExtra, taxRate)
        investInstead.text =
            getString(R.string.currency_symbol) + String.format("%d", investReturn.roundToInt())
        youPayMoneyTextView.text =
            getString(R.string.currency_symbol) + String.format("%d", realTotalPayments.roundToInt())
        youSaveMoneyTextView.text =
            getString(R.string.currency_symbol) + String.format("%d", (maxTotalPayments - realTotalPayments).roundToInt())


        val termUnitText = getTermUnitQuantityString(fullTermUnits)
        val fmt = if (monthsPerTermUnit == 1 || months == 0){
            "%d %s"
        } else{
            //TODO: "mo" should not be a hardcoded string
            "%d %s, %d mo"
        }
        newTermTextView.text = String.format(fmt, fullTermUnits, termUnitText, months)

        return payment
    }

    private fun prettyNumber(rawNumber : String, longZero: Boolean =true): String {

        if(rawNumber == ""){
            return ""
        }

        val cleanedNumber = numberRemoveSeperators(rawNumber)
        val number = try {
            cleanedNumber.toDouble()
        }catch (_: java.lang.NumberFormatException){
            val firstPoint = cleanedNumber.indexOf('.')
            cleanedNumber.subSequence(0, firstPoint).toString().toDouble()
        }

        // Handles "00000" being converted to 0
        // Ex: replacing 200,000 -> 00,000 -> 300,000
        return if(number == 0.0 && longZero) {
            "0".repeat(rawNumber.replace(Regex("[^0-9]"), "").length)
        }else{
            String.format("%,.0f", number)
        }
    }

    private val addSeparatorTextWatcher = object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            val numberString = editable.toString()
            val prettyNum = prettyNumber(numberString)
            if(numberString != prettyNum){
                editable?.replace(0, editable.length, prettyNum)
            }
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            updatePayment()
        }
    }

    private val recalculateTextWatcher = object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) { }
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            updatePayment()
        }
    }

    private fun setStringsByMode(){

        Log.d(TAG, mode?.let{
            "[setStringsByMode] mode value was $it"
        } ?:
            "[setStringsByMode] mode value was null, assuming $defaultMode"
        )

        val savedData = activity?.let{
            LoanData(sharedPref, it).retrieveSavedData(mode ?: defaultMode)
        } ?: LoanData.LoanDataDAO()


        when(mode ?: defaultMode){
            resources.getString(R.string.form_mode_mortgage) -> {

                // Pull this out so we can do QuantityString stuff with it, too
                val defaultTerms = resources.getString(R.string.form_default_mortgage_term)
                val termUnit = getTermUnitQuantityString(defaultTerms.toInt())

                titleText.text = resources.getString(
                    R.string.form_lbl_format_name,
                    resources.getString(R.string.form_mortgage_name)
                )



                savedData.principal?.let {
                    principalNumberEdit.setText(it.toString())
                } ?: principalNumberEdit.setText(R.string.form_default_mortgage_principal)

                savedData.interestRate?.let{
                    rateNumberEdit.setText(it.toString())
                } ?: rateNumberEdit.setText(R.string.form_default_mortgage_apr)
                rateNumberEdit.hint = resources.getString(R.string.form_hint_format_rate, "Mortgage")

                savedData.terms?.let{
                    termNumberEdit.setText(it.roundToInt().toString())
                } ?: termNumberEdit.setText(defaultTerms)
                termNumberEdit.hint = resources.getString(R.string.form_hint_format_term, termUnit)

                savedData.extraPayment?.let{
                    extraPmtEdit.setText(it.toString())
                } ?: extraPmtEdit.setText(R.string.form_default_mortgage_extra_payment)


                termText.setText(R.string.form_lbl_mortgage_term_unit)


            }
            resources.getString(R.string.form_mode_auto) -> {
                val defaultTerms = resources.getString(R.string.form_default_auto_term)
                val termUnit = getTermUnitQuantityString(defaultTerms.toInt())

                titleText.text = resources.getString(
                    R.string.form_lbl_format_name,
                    resources.getString(R.string.form_auto_name)
                )

                savedData.principal?.let {
                    principalNumberEdit.setText(it.toString())
                } ?: principalNumberEdit.setText(R.string.form_default_auto_principal)

                savedData.interestRate?.let{
                    rateNumberEdit.setText(it.toString())
                } ?: rateNumberEdit.setText(R.string.form_default_auto_apr)
                rateNumberEdit.hint = resources.getString(R.string.form_hint_format_rate, "Mortgage")

                savedData.terms?.let{
                    termNumberEdit.setText(it.toString())
                } ?: termNumberEdit.setText(defaultTerms)
                termNumberEdit.hint = resources.getString(R.string.form_hint_format_term, termUnit)

                savedData.extraPayment?.let{
                    extraPmtEdit.setText(it.toString())
                } ?: extraPmtEdit.setText(R.string.form_default_auto_extra_payment)

                termText.setText(R.string.form_lbl_auto_term_unit)
            }
            else -> {
                Log.e(TAG, "unrecognized mode $mode, can't update strings")
            }
        }
    }
}