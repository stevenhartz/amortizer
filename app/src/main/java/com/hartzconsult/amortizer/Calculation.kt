package com.hartzconsult.amortizer

import kotlin.math.*

// Calculates payments for the given parameters.
// @param principal     loan principal
// @param interestRate  should be interest per period, usually (annual_rate / periods_per_year)
// @param periods       interest/payment periods
fun calcPayment(principal : Double, interestRate: Double, periods: Double) : Double{


    val term1 = (1 + interestRate).pow(periods)
    val numerator = interestRate * term1
    val denominator = term1 - 1

    return principal * numerator / denominator
}

// Calculates a loan term for the given parameters. Useful for
// calculating how long loans are with extra payments
// @param principal     loan principal
// @param interestRate  should be interest per period, usually (annual_rate / periods_per_year)
// @param payment       payment per period
fun calcTerm(principal : Double, interestRate: Double, payment: Double) : Double?{

    val a = ln(payment / (payment - interestRate * principal))
    val b = ln(1 + interestRate)

    var numTerms = a / b

    if(numTerms.isNaN())
        return null

    // The `if` logic fixes a minor round-off error
    return numTerms

}

fun annualAPYtoMonthlyRate(annualAPY: Double): Double{
    return exp(ln(1.0 + annualAPY) / 12.0) - 1.0
}
// Calculate investment return for the given parameters, less text
// @param initial           Initial investment
// @param monthly           Monthly additional investment
// @param annualReturnRate  Annual return on investment (rate)
// @param taxRate           Tax rate
fun calcInvestmentWithTax(initial: Double, monthly: Double, annualReturnRate: Double, months: Int, taxRate: Double) : Double{

    val rate = annualAPYtoMonthlyRate(annualReturnRate)
    val termInitial = initial * (1 + rate).pow(months)
    val termMonthly = monthly / rate * ((1 + rate).pow(months) - 1.0)

    val investmentReturn = termInitial + termMonthly
    return (1.0 - taxRate) * investmentReturn
}