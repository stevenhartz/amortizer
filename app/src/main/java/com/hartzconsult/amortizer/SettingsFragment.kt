package com.hartzconsult.amortizer

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat

class SettingsFragment : PreferenceFragmentCompat() {

    private val TAG = "Settings"

    val prefChange =
        SharedPreferences.OnSharedPreferenceChangeListener { sharedPref, key ->
            Log.d(TAG, "A preference changed: $key")

            when(key){
                resources.getString(R.string.sp_save_loan_info) -> {
                    if(!sharedPref.getBoolean(key, false)) {
                        promptWipeLoanData(sharedPref)
                    }
                }

                resources.getString(R.string.sp_save_tax_info) -> {
                    if(!sharedPref.getBoolean(key, false)){
                        setTaxEnabled(false)
                        promptWipeTaxData(sharedPref)
                    }else{
                        setTaxEnabled(true)
                    }
                }

            }
        }

    private fun promptWipeLoanData(sharedPref: SharedPreferences){
        val alertDialog = AlertDialog.Builder(activity).create()
        alertDialog.setTitle(R.string.delete_loan_title)
        alertDialog.setMessage(getString(R.string.delete_loan_message))
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.delete)) { dialog, _ ->
            LoanData(sharedPref, requireContext()).purgeLoanData()
            dialog.dismiss()
        }

        alertDialog.setButton(
            AlertDialog.BUTTON_NEGATIVE,
            getString(R.string.leave)
        ) { dialog, _ -> dialog.dismiss() }

        alertDialog.show()
    }

    private fun promptWipeTaxData(sharedPref: SharedPreferences){
        val alertDialog = AlertDialog.Builder(activity).create()
        alertDialog.setTitle(R.string.delete_tax_title)
        alertDialog.setMessage(getString(R.string.delete_tax_message))
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.delete)) { dialog, _ ->
            with(sharedPref.edit()){
                remove(getString(R.string.sp_tax_rate))
                commit()
            }
        }

        alertDialog.setButton(
            AlertDialog.BUTTON_NEGATIVE,
            getString(R.string.leave)
        ) { dialog, _ -> dialog.dismiss() }

        alertDialog.show()
    }

    private fun setTaxEnabled(b: Boolean){
        val taxSetting = preferenceManager.findPreference<Preference>(getString(R.string.sp_tax_rate))
        taxSetting?.isEnabled = b
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
    }

    override fun onResume() {
        super.onResume()

        val sharedPref = preferenceManager.sharedPreferences

        val value = sharedPref.getBoolean(getString(R.string.sp_save_tax_info),false)
        setTaxEnabled(value)

        sharedPref.registerOnSharedPreferenceChangeListener(prefChange)
    }

    override fun onPause() {
        super.onPause()
        preferenceScreen.sharedPreferences.unregisterOnSharedPreferenceChangeListener(prefChange)
    }

}