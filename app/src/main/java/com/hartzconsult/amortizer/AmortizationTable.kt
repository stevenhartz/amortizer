package com.hartzconsult.amortizer

import android.content.Intent
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.lang.NullPointerException
import kotlin.math.max
import kotlin.math.roundToInt

class AmortizationTable : AppCompatActivity() {

    // Kotlin's version of static fields
    companion object {
        const val INTENT_PRINCIPAL = "principal"
        const val INTENT_RATE = "rate"
        const val INTENT_YEARS = "years"
        const val INTENT_PAYMENT = "payment"
    }


    private val amData = AmortizationData.DataList()
    private var amDataReady: Boolean = false
    private val widthArray = Array<Int>(4) {0}

    private lateinit var recyclerView : RecyclerView
    private lateinit var progBar : ProgressBar
    private lateinit var hdrLayout: LinearLayout
    private lateinit var hdrPeriod: TextView
    private lateinit var hdrBalance: TextView
    private lateinit var hdrInterest: TextView
    private lateinit var hdrPrincipal: TextView

    private var principal : Double? = null
    private var rate : Double? = null
    private var years : Double? = null
    private var payment: Double? = null

    private lateinit var table: TableLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_amortization_table)

        getReferences()

        // This doesn't seem to stay set from the layout.xml, so just set it again
        hdrLayout.visibility = View.INVISIBLE

        this.extractExtras(intent.extras)
        this.calculateAmortization()

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.amortization_table_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when(item.itemId){
            R.id.show_graph ->{

                object: Runnable{
                    override fun run() {
                        if(!amDataReady){
                            Handler(Looper.getMainLooper()).postDelayed({
                                this
                            }, 100)
                        }else{
                            launchGrapher()
                        }
                    }
                }.run()

                Log.v("ShowGraphBtn", "Show Graph clicked")
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun getReferences(){
        progBar = findViewById<ProgressBar>(R.id.AmoTableProgressBar)
        recyclerView = findViewById(R.id.amoTableRecycler)
        hdrLayout = findViewById<LinearLayout>(R.id.hdrLayout)
        hdrPeriod = findViewById(R.id.hdrPeriodTextView)
        hdrBalance = findViewById(R.id.hdrBalanceTextView)
        hdrInterest = findViewById(R.id.hdrInterestTextView)
        hdrPrincipal = findViewById(R.id.hdrPrincipalTextView)
    }

    private fun launchGrapher(){
        if(!amDataReady){
            Log.e("LaunchGrapher", "Can't launch grapher without any data. Wait for data to be ready")
            Toast.makeText(this, "Error, data not ready (try again in a moment)", Toast.LENGTH_SHORT)
        }else{
            val intent = Intent(this@AmortizationTable, Grapher::class.java)

            intent.putExtra("amortizationData", amData)

            startActivity(intent)
        }
    }

    private fun extractExtras(extras: Bundle?){

        extras?.let{

            principal = it.getDouble(INTENT_PRINCIPAL)
            rate = it.getDouble(INTENT_RATE)
            years = it.getDouble(INTENT_YEARS)
            payment = it.getDouble(INTENT_PAYMENT)
            principal = it.getDouble(INTENT_PRINCIPAL)

        }

        try{
            // We need these not-null assertions, might as well keep the logging
            Log.v("AmoTableValues",principal!!.toString())
            Log.v("AmoTableValues",rate!!.toString())
            Log.v("AmoTableValues",years!!.toString())
            Log.v("AmoTableValues",payment!!.toString())
        }catch (e: NullPointerException){
            Log.e("AmoTable", "At least one of the 'extras' was null!")
            Toast.makeText(this, "An error occurred :c", Toast.LENGTH_SHORT).show()
        }


    }

    private fun calculateAmortization() {

        Log.i("TAG", "Calculating")
        amDataReady = false



        
        ThreadWithCallback(Thread {
            Log.i("TAG", "running thread!")
            try {
                // Assert not-null because we can't recover if there are null
                var balance = principal!!
                val _rate = rate!!.let {
                    var correctedRate = it
                    if (correctedRate > 1.0) {
                        correctedRate /= 100.0
                    }
                    correctedRate / 12.0
                }
                var _payment = payment!!

                var period: Int = 0

                // Add "empty" first row at very start of loan (when first taken out)

                amData.list.add(
                    AmortizationData(
                        balance,
                        -1.0,
                        -1.0
                    )
                )


                while (balance > 0) {
                    period += 1

                    if (period > 1000) {
                        break
                    }
                    var newBalance = (balance + balance * _rate - _payment)
                    if (newBalance > 0) {
                        // Force to whole cents; this is a weak fixed-point number conversion
                        newBalance = (newBalance * 100).roundToInt() / 100.0
                    } else {
                        newBalance = 0.0
                        _payment = balance - newBalance
                    }
                    val balanceChange = balance - newBalance
                    var principalPayment = balanceChange
                    var interestPayment = _payment - principalPayment

                    balance = newBalance

                    amData.list.add(
                        AmortizationData(
                            balance,
                            interestPayment,
                            principalPayment
                        )
                    )

                    val balChars = String.format(getString(R.string.fmt_currency), balance).length
                    val intChars = String.format(getString(R.string.fmt_small_currency), interestPayment).length
                    val prinChars = String.format(getString(R.string.fmt_small_currency), principalPayment).length

                    if(balChars > widthArray[1]){
                        widthArray[1] = balChars
                    }
                    if(intChars > widthArray[2]){
                        widthArray[2] = intChars
                    }
                    if(prinChars > widthArray[3]){
                        widthArray[3] = prinChars
                    }
                }
            } catch (e: NullPointerException) {
                Toast.makeText(this, "Failed to perform the calculation", Toast.LENGTH_SHORT)
                Log.e("AmoTable", "calculation values were null")
            }

            widthArray[0] = amData.list.size.toString().length

        },
        // Added parameter name to just linter up; this is unreadable if
        // callback lambda is moved outside parens.
        callback={

            this.runOnUiThread{
                this.amDataReady = true
                this.addTableToView()
            }
        })



    }

    private fun addTableToView(){

        // Header text can be longer than displayed numbers, so we need to fix widthArray
        widthArray[0] = max(widthArray[0], hdrPeriod.text.length)
        widthArray[1] = max(widthArray[1], hdrBalance.text.length)
        widthArray[2] = max(widthArray[2], hdrInterest.text.length)
        widthArray[3] = max(widthArray[3], hdrPrincipal.text.length)

        recyclerView.adapter = AmortizationTableAdapter(
            this,
            amData.list.toTypedArray(),
            widthArray
        )

        recyclerView.layoutManager = LinearLayoutManager(this)


        (progBar.parent as ViewGroup).removeView(progBar)


        // Match header widths to data widths, then make this visible
        val widths = AmortizationTableAdapter.calcWidths(this.widthArray, recyclerView.width)
        hdrPeriod.layoutParams.width = widths[0]
        hdrBalance.layoutParams.width = widths[1]
        hdrInterest.layoutParams.width = widths[2]
        hdrPrincipal.layoutParams.width = widths[3]
        hdrLayout.visibility = View.VISIBLE

    }

}