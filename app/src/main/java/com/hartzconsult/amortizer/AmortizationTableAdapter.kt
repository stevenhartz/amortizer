package com.hartzconsult.amortizer

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.roundToInt

class AmortizationTableAdapter(context: Context, private val dataset: Array<AmortizationData>, private val widths: Array<Int>) :
    RecyclerView.Adapter<AmortizationTableAdapter.Holder>() {

    private val inflater = LayoutInflater.from(context)
    private var parentWidth: Int = 1

    private lateinit var context: Context

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView){


        val periodTextView = itemView.findViewById<TextView>(R.id.amo_table_period)
        val balanceTextView = itemView.findViewById<TextView>(R.id.amo_table_balance)
        val interestTextView = itemView.findViewById<TextView>(R.id.amo_table_interest)
        val principalTextView = itemView.findViewById<TextView>(R.id.amo_table_principal)
    }

    // Kotlin's way of making static functions which still reside in this class
    companion object{
        fun calcWidths(relativeWidths: Array<Int>, parentWidth: Int): Array<Int>{
            val widthPx = relativeWidths.map{
                (it.toDouble() / relativeWidths.sum() * parentWidth).roundToInt()
            }.toTypedArray()


            // Fix round-over issues
            if (widthPx.sum() != parentWidth){
                // Will "add" if widthPx.sum() is less than parentWidth
                widthPx[0] -= widthPx.sum() - parentWidth
            }

            return widthPx
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        context = parent.context

        val v = inflater.inflate(R.layout.amortization_table_item, parent, false)
        parentWidth = parent.width
        return Holder(v)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val widthPx = calcWidths(this.widths, parentWidth)

        // People expect loans to start at month 1 not 0, so add one here.
        holder.periodTextView.text = String.format("%d", position + 1)
        holder.periodTextView.layoutParams.width = widthPx[0]


        holder.balanceTextView.text = String.format(
            context.getString(R.string.fmt_currency), dataset[position].balance)
        holder.balanceTextView.layoutParams.width = widthPx[1]

        if(dataset[position].interestPayment >= 0) {
            holder.interestTextView.text = String.format(
                context.getString(R.string.fmt_small_currency), dataset[position].interestPayment
            )
        }else{
            holder.interestTextView.text = "--"
        }
        holder.interestTextView.layoutParams.width = widthPx[2]

        if(dataset[position].principalPayment >= 0) {
            holder.principalTextView.text = String.format(
                context.getString(R.string.fmt_small_currency), dataset[position].principalPayment
            )
        }else{
            holder.principalTextView.text = "--"
        }
        holder.principalTextView.layoutParams.width = widthPx[3]

    }

    override fun getItemCount(): Int {
        return dataset.size
    }

}