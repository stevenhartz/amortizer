package com.hartzconsult.amortizer

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.lang.NullPointerException
import kotlin.math.max
import kotlin.math.roundToInt
import android.view.*
import androidx.navigation.fragment.findNavController

class AmortizationTableFragment : Fragment() {
    private val TAG = "Auto"

    // TODO: Rename and change types of parameters
    //private var param1: String? = null
    //private var param2: String? = null


    // Kotlin's version of static fields
    companion object {
        const val INTENT_PRINCIPAL = "principal"
        const val INTENT_RATE = "rate"
        const val INTENT_TERMS = "years"
        const val INTENT_PAYMENT = "payment"
    }


    private val amData = AmortizationData.DataList()
    private var amDataReady: Boolean = false
    private val widthArray = Array<Int>(4) {0}

    private lateinit var recyclerView : RecyclerView
    private lateinit var progBar : ProgressBar
    private lateinit var hdrLayout: LinearLayout
    private lateinit var hdrPeriod: TextView
    private lateinit var hdrBalance: TextView
    private lateinit var hdrInterest: TextView
    private lateinit var hdrPrincipal: TextView

    private var principal : Double? = null
    private var rate : Double? = null
    private var years : Double? = null
    private var payment: Double? = null

    private lateinit var table: TableLayout

    private lateinit var myView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            getData(it)
        }

        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        myView =  inflater.inflate(R.layout.activity_amortization_table, container, false)
        return myView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getReferences()

        this.calculateAmortization()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.amortization_table_menu, menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when(item.itemId){
            R.id.show_graph ->{

                launchGrapher()

                Log.v("ShowGraphBtn", "Show Graph clicked")
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun getReferences(){
        progBar = myView.findViewById<ProgressBar>(R.id.AmoTableProgressBar)
        recyclerView = myView.findViewById(R.id.amoTableRecycler)
        hdrLayout = myView.findViewById<LinearLayout>(R.id.hdrLayout)
        hdrPeriod = myView.findViewById(R.id.hdrPeriodTextView)
        hdrBalance = myView.findViewById(R.id.hdrBalanceTextView)
        hdrInterest = myView.findViewById(R.id.hdrInterestTextView)
        hdrPrincipal = myView.findViewById(R.id.hdrPrincipalTextView)
    }

    private fun launchGrapher(){
        if(!amDataReady){
            Log.e("LaunchGrapher", "Can't launch grapher without any data. Wait for data to be ready")
            Toast.makeText(activity, "Error, data not ready (try again in a moment)", Toast.LENGTH_SHORT)
        }else{
            val navController = this.findNavController()
            navController.navigate(R.id.grapherFragment, Bundle().apply{
                this.putParcelable("amortizationData", amData)
            })
        }
    }

    private fun getData(data: Bundle?){

        data?.let{

            rate = it.getDouble(INTENT_RATE)
            years = it.getDouble(INTENT_TERMS)
            payment = it.getDouble(INTENT_PAYMENT)
            principal = it.getDouble(INTENT_PRINCIPAL)

        }

        try{
            // We need these not-null assertions, might as well keep the logging
            Log.v("AmoTableValues",principal!!.toString())
            Log.v("AmoTableValues",rate!!.toString())
            Log.v("AmoTableValues",years!!.toString())
            Log.v("AmoTableValues",payment!!.toString())
        }catch (e: NullPointerException){
            Log.e("AmoTable", "At least one of the 'extras' was null!")
            Toast.makeText(activity, "An error occurred :c", Toast.LENGTH_SHORT).show()
        }


    }

    private fun calculateAmortization() {

        if(amDataReady){
            Log.d(TAG, "[calculateAmortization] data already calculated; skipping.")
            this.addTableToView()
            return
        }

        Log.i(TAG, "[calculateAmortization] Calculating")


        amDataReady = false

        ThreadWithCallback(Thread {
            Log.i("TAG", "running thread!")
            try {
                // Assert not-null because we can't recover if there are null
                var balance = principal!!
                val _rate = rate!!.let {
                    var correctedRate = it
                    if (correctedRate > 1.0) {
                        correctedRate /= 100.0
                    }
                    correctedRate / 12.0
                }
                var _payment = payment!!

                var period = 0
                while (balance > 0) {

                    val startingBalance = balance

                    // 4800 months = 200 years; it's unreasonable for a legitimate loan to run this long
                    if (++period > 4800) {
                        Log.d(TAG, "calculateAmortization: safety tripped, excessive term length!")
                        break
                    }
                    var newBalance = (startingBalance + (startingBalance * _rate) - _payment)
                    if (newBalance > 0) {
                        // Force to whole cents; this is a weak fixed-point number conversion
                        newBalance = (newBalance * 100).roundToInt() / 100.0
                    } else {
                        newBalance = 0.0
                        _payment = startingBalance - newBalance
                    }
                    val balanceChange = startingBalance - newBalance
                    var principalPayment = balanceChange
                    var interestPayment = _payment - principalPayment

                    // Updating running balance
                    balance = newBalance

                    amData.list.add(
                        AmortizationData(
                            startingBalance,
                            interestPayment,
                            principalPayment
                        )
                    )

                    val balChars = String.format(getString(R.string.fmt_currency), balance).length
                    val intChars = String.format(getString(R.string.fmt_small_currency), interestPayment).length
                    val prinChars = String.format(getString(R.string.fmt_small_currency), principalPayment).length

                    if(balChars > widthArray[1]){
                        widthArray[1] = balChars
                    }
                    if(intChars > widthArray[2]){
                        widthArray[2] = intChars
                    }
                    if(prinChars > widthArray[3]){
                        widthArray[3] = prinChars
                    }
                }

                // Add final "0 starting balance" point
                amData.list.add(AmortizationData(
                    0.0,
                    -1.0,
                    -1.0
                ))
            } catch (e: NullPointerException) {
                Toast.makeText(activity, "Failed to perform the calculation", Toast.LENGTH_SHORT)
                Log.e("AmoTable", "calculation values were null")
            }

            widthArray[0] = amData.list.size.toString().length

        },
            // Added parameter name to just linter up; this is unreadable if
            // callback lambda is moved outside parens.
            callback={

                activity?.runOnUiThread{
                    this.amDataReady = true
                    this.addTableToView()
                }
            })



    }

    private fun addTableToView(){

        // Header text can be longer than displayed numbers, so we need to fix widthArray
        widthArray[0] = max(widthArray[0], hdrPeriod.text.length)
        widthArray[1] = max(widthArray[1], hdrBalance.text.length)
        widthArray[2] = max(widthArray[2], hdrInterest.text.length)
        widthArray[3] = max(widthArray[3], hdrPrincipal.text.length)

        recyclerView.adapter = AmortizationTableAdapter(
            myView.context,
            amData.list.toTypedArray(),
            widthArray
        )

        recyclerView.layoutManager = LinearLayoutManager(myView.context)


        (progBar.parent as ViewGroup).removeView(progBar)


        // Match header widths to data widths, then make this visible
        val widths = AmortizationTableAdapter.calcWidths(this.widthArray, recyclerView.width)
        hdrPeriod.layoutParams.width = widths[0]
        hdrBalance.layoutParams.width = widths[1]
        hdrInterest.layoutParams.width = widths[2]
        hdrPrincipal.layoutParams.width = widths[3]
        hdrLayout.visibility = View.VISIBLE

    }
}