package com.hartzconsult.amortizer
class ThreadWithCallback(t: Thread, callback: () -> Unit){

    init {
        Thread {
            t.run()
            callback()
        }.start()
    }
}