package com.hartzconsult.amortizer

import android.graphics.Canvas
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import java.lang.NullPointerException
import kotlin.math.max
import kotlin.math.roundToInt

/**
 * A simple [Fragment] subclass.
 * Use the [GrapherFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class GrapherFragment : Fragment() {

    // Yes, I hate myself too
    class PlotDAO(
        var balanceData: FloatArray,
        var interestData: FloatArray,
        var principalData: FloatArray
    )

    class PlotOffsets(
        var left: Float = 0F,
        var right: Float = 0F,
        var top: Float = 0F,
        var bottom: Float = 0F
    )

    private lateinit var surfView : SurfaceView
    private lateinit var amData : AmortizationData.DataList
    private val painter = Paint()
    private var fontSize: Float = 0F
    private var graphTextPadding: Float = 0F
    private var graphTopBottomPadding = 0F
    private var axesTicks: Int = 0
    private var tickSize: Float = 0F

    private var maxBalance = 0F
    private var maxInterestOrPrincipal = 0F

    private val plotSpaceOffsets: PlotOffsets = PlotOffsets()
    private val axisTitleOffsets: PlotOffsets = PlotOffsets()
    private val verticalAxisTickOffsets: PlotOffsets = PlotOffsets()

    private var verticalTickPositions: MutableList<Float>? = null
    private var horizontalTickPositions:  MutableList<Float>? = null

    private lateinit var myView: View


    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {


            try {
                amData = it.getParcelable("amortizationData")!!

                Log.v(
                    "GrapherExtras",
                    "First recovered: ${amData.list[0].balance}, ${amData.list[0].interestPayment}, ${amData.list[0].principalPayment}"
                )
                Log.v(
                    "GrapherExtras",
                    "Last recovered: ${amData.list.last().balance}, ${amData.list.last().interestPayment}, ${amData.list.last().principalPayment}"
                )
            } catch (e: NullPointerException) {
                Log.e("Grapher-unpack", "Couldn't extract data :(")
                Log.d("Grapher", "I'm here for the popcorn")
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.activity_grapher, container, false)
        return myView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        getResourceValues()


        activity?.let {activity ->
            surfView = activity.findViewById(R.id.surfaceView)
            surfView.holder.addCallback(object : SurfaceHolder.Callback {
                override fun surfaceCreated(holder: SurfaceHolder) {
                    drawGraph(holder)
                }

                override fun surfaceChanged(holder: SurfaceHolder, p1: Int, p2: Int, p3: Int) {

                }

                override fun surfaceDestroyed(p0: SurfaceHolder) {
                    //Don't do anything yet
                }
            })
        }
    }

    // MUST NOT be called before activity is created!
    private fun getResourceValues(){
        fontSize = resources.getDimension(R.dimen.graph_font_size)
        painter.strokeWidth = resources.getDimension(R.dimen.graph_line_width)
        painter.textSize = fontSize
        graphTextPadding = resources.getDimension(R.dimen.graph_text_padding)
        graphTopBottomPadding = resources.getDimension(R.dimen.graph_top_bottom_padding)
        axesTicks = resources.getInteger(R.integer.ticks)
        tickSize = resources.getDimension(R.dimen.tick_size)
    }


    // Should help make sure I never forget canvas.restore() and also makes the "rotated block" clearer
    private fun canvasRotate(canvas: Canvas, rotation: Float, px: Float, py: Float, r: ()->Unit){
        canvas.save()
        canvas.rotate(rotation, px, py)
        r()
        canvas.restore()
    }

    private fun calcOffsets(){
        axisTitleOffsets.apply {
            left = graphTextPadding + fontSize
            right = graphTextPadding
            top = graphTopBottomPadding
            bottom = graphTopBottomPadding
        }

        verticalAxisTickOffsets.apply {
            left = axisTitleOffsets.left + fontSize + graphTextPadding
            right = axisTitleOffsets.right + fontSize + graphTextPadding
            top = graphTopBottomPadding
            bottom = graphTopBottomPadding + 2 * fontSize + 4 * graphTextPadding
        }

        plotSpaceOffsets.apply {
            left = verticalAxisTickOffsets.left + fontSize + graphTextPadding
            right = verticalAxisTickOffsets.right + fontSize + graphTextPadding
            top = graphTopBottomPadding
            bottom = graphTopBottomPadding+ 2 * fontSize + 4 * graphTextPadding
        }
    }

    // Calculates tick positions. Canvas object needed for dimensions
    private fun calcTickPositions(canvas: Canvas){
        verticalTickPositions = MutableList(axesTicks) {i ->

            // This plots the 0th tick at the BOTTOM
            val tickPos = canvas.height - ( i.toFloat() / (axesTicks.toFloat()-1)
                    * (canvas.height - verticalAxisTickOffsets.top - verticalAxisTickOffsets.bottom)
                    + verticalAxisTickOffsets.bottom)

            tickPos
        }

        horizontalTickPositions = MutableList(axesTicks) { i ->

            val tickPos = (i.toFloat() / (axesTicks.toFloat() - 1)
                    * (canvas.width - plotSpaceOffsets.left - plotSpaceOffsets.right)
                    + plotSpaceOffsets.left)

            tickPos
        }

    }

    private fun drawGraph(holder: SurfaceHolder){
        calcOffsets()

        val canvas = holder.lockCanvas()
        val width = canvas.width
        val height = canvas.height

        calcTickPositions(canvas)

        val plotData = prepDataForPlotting()
        scaleAllDataInplace(plotData, width.toFloat(), height.toFloat())

        canvas.drawColor(resources.getColor(R.color.graph_background, activity?.theme))

        drawGridlines(canvas)

        // Run plotData before drawAxes to fix first-datapoint-is-0 lines
        plotData(canvas, plotData)
        drawAxes(canvas)

        // TODO: If block to allow people to disable this?
        // TODO: run this before plotData so that gridlines don't overlay data? REQUIRES REFACTOR


        drawLineLabels(canvas)
        holder.unlockCanvasAndPost(canvas)
    }

    private fun scaleAllDataInplace(plots: Grapher.PlotDAO, width: Float, height: Float){

        scaleDataInplace(plots.balanceData, width.toFloat(), height.toFloat(), plotSpaceOffsets)
        scaleDataInplace(plots.interestData, width.toFloat(), height.toFloat(), plotSpaceOffsets)
        scaleDataInplace(plots.principalData, width.toFloat(), height.toFloat(), plotSpaceOffsets)
    }

    private fun drawVerticalAxes(canvas: Canvas){
        val width = canvas.width
        val height = canvas.height

        painter.color = resources.getColor(R.color.axes_color, activity?.theme)
        painter.textAlign = Paint.Align.CENTER

        val rot = -90F
        val centerHeight = height / 2F

        val balanceStr = resources.getString(R.string.graph_lbl_balance)
        val currSym = resources.getString(R.string.currency_symbol)

        var axisThousands = maxBalance > 9999

        canvasRotate(canvas, rot, axisTitleOffsets.left, centerHeight) {
            canvas.drawText(balanceStr, axisTitleOffsets.left, centerHeight, painter)
        }

        canvasRotate(canvas, rot, width - axisTitleOffsets.right, centerHeight) {
            canvas.drawText(
                "Interest & Principal Payment",
                width - axisTitleOffsets.right, centerHeight, painter
            )
        }

        painter.color = resources.getColor(R.color.lines_balance, activity?.theme)
        canvas.drawLine(
            plotSpaceOffsets.left,
            plotSpaceOffsets.top,
            plotSpaceOffsets.left,
            height.toFloat() - plotSpaceOffsets.bottom,
            painter
        )

        canvas.drawLine(width - plotSpaceOffsets.right,
            plotSpaceOffsets.top,
            width - plotSpaceOffsets.right,
            height.toFloat() - plotSpaceOffsets.bottom,
            painter
        )

        try {
            verticalTickPositions!!.forEachIndexed { i, tickPos ->

                canvas.drawLine(
                    plotSpaceOffsets.left,
                    tickPos,
                    plotSpaceOffsets.left - tickSize,
                    tickPos,
                    painter
                )
                canvasRotate(canvas, -90F, verticalAxisTickOffsets.left, tickPos) {
                    var balanceTickValue = maxBalance / (axesTicks - 1) * i
                    if (axisThousands) {
                        balanceTickValue /= 1000F
                    }
                    painter.textAlign = when (i) {
                        0 -> Paint.Align.LEFT
                        axesTicks - 1 -> Paint.Align.RIGHT
                        else -> Paint.Align.CENTER
                    }
                    canvas.drawText(
                        if (axisThousands) {
                            "${currSym}${balanceTickValue.roundToInt()}k"
                        } else {
                            "${currSym}${balanceTickValue.roundToInt()}"
                        },
                        verticalAxisTickOffsets.left, tickPos, painter
                    )
                }

                canvas.drawLine(
                    width - plotSpaceOffsets.right,
                    tickPos,
                    width - plotSpaceOffsets.right + tickSize,
                    tickPos,
                    painter
                )
                canvasRotate(canvas, -90F, width - verticalAxisTickOffsets.right, tickPos) {
                    val intPrinTickValue = maxInterestOrPrincipal / (axesTicks - 1) * i
                    painter.textAlign = when (i) {
                        0 -> Paint.Align.LEFT
                        axesTicks - 1 -> {
                            Log.i("Grapher", "Setting to right align"); Paint.Align.RIGHT
                        }
                        else -> Paint.Align.CENTER
                    }

                    canvas.drawText(
                        "${currSym}${intPrinTickValue.roundToInt()}",
                        width - verticalAxisTickOffsets.right, tickPos, painter
                    )
                }
            }
        }catch (e: NullPointerException){
            Log.e("Grapher.drawVerticalAxes","Vertical tick positions not calculated, cannot run")
        }
    }

    private fun drawHorizontalAxes(canvas: Canvas){
        val width = canvas.width
        val height = canvas.height

        painter.textAlign = Paint.Align.CENTER
        painter.color = resources.getColor(R.color.axes_color, activity?.theme)
        canvas.drawText(
            "Payment Period",
            width / 2F,
            height - graphTopBottomPadding - graphTextPadding,
            painter
        )

        canvas.drawLine(plotSpaceOffsets.left, height - plotSpaceOffsets.bottom,
            width - plotSpaceOffsets.right, height - plotSpaceOffsets.bottom,
            painter
        )

        try {
            horizontalTickPositions!!.forEachIndexed { i, tickPos ->

                Log.d("HorizAxis", "tickPos = $tickPos")

                canvas.drawLine(
                    tickPos, height - plotSpaceOffsets.bottom, tickPos,
                    height - plotSpaceOffsets.bottom + tickSize, painter
                )

                painter.textAlign = when (i) {
                    0 -> Paint.Align.LEFT
                    axesTicks - 1 -> Paint.Align.RIGHT
                    else -> Paint.Align.CENTER
                }
                canvas.drawText(
                    // list.count()-1 because there are always n+1 elements at the moment
                    // axesTicks-1 because i ranges from [0..axesTicks-1]
                    // Then add 1 so that numbers range from 1..totalMonths not 0..totalMonths-1
                    (i * (amData.list.count() - 1).toFloat() / (axesTicks - 1) + 1).roundToInt().toString(),
                    tickPos, height - plotSpaceOffsets.bottom + fontSize + graphTextPadding,
                    painter
                )

            }
        }catch(e: NullPointerException){
            Log.e("Grapher.drawHorizontalAxes","Horizontal tick positions not calculated, cannot run")
        }
    }

    private fun drawAxes(canvas: Canvas){
        drawVerticalAxes(canvas)
        drawHorizontalAxes(canvas)
    }

    private fun drawGridlines(canvas: Canvas){

        // TODO: Make this a resource value?
        painter.color = resources.getColor(R.color.gridline_color, activity?.theme)

        try{
            // Assert both before drawing *anything*. We need both to be non-null to draw a full
            // grid. Don't draw half a grid if one is set but not the other.
            verticalTickPositions!!.let{ vertical ->
                horizontalTickPositions!!.let{ horizontal ->
                    for(tickPos in vertical.drop(1)){
                        canvas.drawLine(plotSpaceOffsets.left, tickPos,
                            canvas.width - plotSpaceOffsets.right, tickPos, painter)
                    }

                    for(tickPos in horizontal.drop(1).dropLast(1)){
                        canvas.drawLine(tickPos, plotSpaceOffsets.top,
                            tickPos, canvas.height - plotSpaceOffsets.bottom, painter)
                    }
                }
            }

        }catch (e: NullPointerException){
            Log.e("drawGridlines", "Can't draw a gridline until tick positions are known")
            return
        }
    }

    private fun drawLineLabels(canvas: Canvas){
        /*
        Balance will always start the the top-left.
        Principal should always finish at the top-right (for American-style mortages at least)
        Interest should always finish at bottom-right (but with Balance)
         */

        painter.color = resources.getColor(R.color.axes_color, activity?.theme)
        painter.textAlign = Paint.Align.LEFT

        // This *seems* to work ok for the Interest and Principal Payment lines, in that
        // the text minimally obscures the lines but is still close enough to tell you what it is
        val twelvePercentPlotWidth = ((canvas.width - plotSpaceOffsets.right) -  plotSpaceOffsets.left) * 0.12F
        val twelvePercentPlotHeight = ((canvas.height - plotSpaceOffsets.bottom) -  plotSpaceOffsets.top) * 0.12F

        // 12% does NOT work here, so we're going to hard-code a small offset that shouldn't be
        // an issue on any real device.
        val balanceLabelX = plotSpaceOffsets.left + 10F
        val balanceLabelY = plotSpaceOffsets.top + twelvePercentPlotHeight
        canvas.drawText("Start Balance", balanceLabelX, balanceLabelY, painter)

        painter.color = resources.getColor(R.color.lines_interest, activity?.theme)
        painter.textAlign = Paint.Align.RIGHT
        val interestLabelX = canvas.width - plotSpaceOffsets.right - twelvePercentPlotWidth
        val interestLabelY = canvas.height - plotSpaceOffsets.bottom - twelvePercentPlotHeight
        canvas.drawText("Interest Payment", interestLabelX, interestLabelY, painter)

        painter.color = resources.getColor(R.color.lines_principal, activity?.theme)
        painter.textAlign = Paint.Align.RIGHT
        val principalLabelX = canvas.width - plotSpaceOffsets.right - twelvePercentPlotWidth
        val principalLabelY =plotSpaceOffsets.top + twelvePercentPlotHeight
        canvas.drawText("Principal Payment", principalLabelX, principalLabelY, painter)
    }

    private fun plotData(canvas: Canvas, plots: Grapher.PlotDAO){

        // Plot lines
        painter.color = resources.getColor(R.color.lines_balance, activity?.theme)
        canvas.drawLines(plots.balanceData, painter)

        painter.color = resources.getColor(R.color.lines_interest, activity?.theme)
        canvas.drawLines(plots.interestData, painter)

        painter.color = resources.getColor(R.color.lines_principal, activity?.theme)
        canvas.drawLines(plots.principalData, painter)
    }

    // Converts all data to a 0..1 representation which scales to our plotting surface easily.
    // Note that (0,0) is the TOP LEFT corner of our canvas-graph, so we need to do some work
    // To coerce the data into that format.
    private fun prepDataForPlotting(): Grapher.PlotDAO {
        val balanceData = FloatArray(amData.list.count() * 4)
        val interestData = FloatArray((amData.list.count()-1) * 4)
        val principalData = FloatArray((amData.list.count()-1) * 4)

        // Kotlin doesn't have an easy way to get the max of a sub-element, so now there's this:
        maxBalance = 0F
        maxInterestOrPrincipal = 0F
        for(it in amData.list) {
            maxBalance = max(maxBalance, it.balance.toFloat())

            // Plot interest payments and principal payments on same axis, so they'll need same scale
            maxInterestOrPrincipal = max(
                maxInterestOrPrincipal,
                max(it.principalPayment.toFloat(), it.interestPayment.toFloat())
            )
        }

        // Normalize data
        val dataList = amData.list
        val adjCount = dataList.count()-1
        for(i in 0 until adjCount){
            balanceData[4*i] = i.toFloat() / adjCount
            balanceData[4*i+1] = 1.0F - dataList[i].balance.toFloat() / maxBalance
            balanceData[4*i+2] = (i+1).toFloat()  / adjCount
            balanceData[4*i+3] = 1.0F - dataList[i+1].balance.toFloat() / maxBalance

            //Log.d("GrapherBalance", "b1=${balanceData[4*i+1]}\tb2=${balanceData[4*i+3]}")
            //Log.d("GrapherPeriod", "p1=${balanceData[4*i]}\tp2=${balanceData[4*i+2]}")

            if(i != adjCount - 1) {
                interestData[4 * i] = i.toFloat() / adjCount
                interestData[4 * i + 1] =
                    1.0F - dataList[i].interestPayment.toFloat() / maxInterestOrPrincipal
                interestData[4 * i + 2] = (i + 1).toFloat() / adjCount
                interestData[4 * i + 3] =
                    1.0F - dataList[i + 1].interestPayment.toFloat() / maxInterestOrPrincipal

                principalData[4 * i] = i.toFloat() / adjCount
                principalData[4 * i + 1] =
                    1.0F - dataList[i].principalPayment.toFloat() / maxInterestOrPrincipal
                principalData[4 * i + 2] = (i + 1).toFloat() / adjCount
                principalData[4 * i + 3] =
                    1.0F - dataList[i + 1].principalPayment.toFloat() / maxInterestOrPrincipal
            }
        }

        // I like readable names instead of magic-number indices. Sue me.
        return Grapher.PlotDAO(balanceData, interestData, principalData)
    }

    private fun scaleDataInplace(
        plotData: FloatArray, width: Float, height: Float, padding: PlotOffsets
    ){
        val adjWidthScale = width - padding.left - padding.right
        val adjHeightScale = height - padding.top - padding.bottom
        for(i in 0 until plotData.count() step 2){
            // X value
            plotData[i] = plotData[i] * adjWidthScale + padding.left
            // Y value
            plotData[i+1] = plotData[i+1] * adjHeightScale + padding.top
        }
    }

}