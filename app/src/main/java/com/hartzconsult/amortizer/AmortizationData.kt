package com.hartzconsult.amortizer

import android.os.Parcel
import android.os.Parcelable
import android.util.Log

class AmortizationData (balance: Double, interestPmt: Double, principalPmt: Double): android.os.Parcelable{
    val balance: Double = balance
    val interestPayment: Double = interestPmt
    val principalPayment: Double = principalPmt

    class DataList : android.os.Parcelable{

        var list = mutableListOf<AmortizationData>()
            private set

        constructor()

        private constructor(parcel: Parcel){
            parcel.readList(list, javaClass.classLoader)
        }



        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeList(list)
        }


        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : android.os.Parcelable.Creator<Parcelable> {
            override fun createFromParcel(parcel: Parcel): Parcelable {
                return DataList(parcel)
            }

            override fun newArray(size: Int): Array<Parcelable?> {
                Log.w("AmortizationData.DataList", "newArray isn't meant to work, but was called.")
                return arrayOfNulls(size)
            }
        }

    }

    constructor(parcel: Parcel) : this(
        parcel.readDouble(), parcel.readDouble(), parcel.readDouble()
    )


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(balance)
        parcel.writeDouble(interestPayment)
        parcel.writeDouble(principalPayment)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AmortizationData> {
        override fun createFromParcel(parcel: Parcel): AmortizationData {
            return AmortizationData(parcel)
        }

        override fun newArray(size: Int): Array<AmortizationData?> {
            return arrayOfNulls(size)
        }
    }
}